package br.edu.up;

public interface Comercializavel {
	
	double getValor();
	double getAltura();

}