package br.edu.up;

import java.util.ArrayList;
import java.util.List;

//2a forma: implementação em classe hospedeira
public class Programa implements OnTouchListener {

	public static void main(String[] args) throws Exception {

		Programa hospedeira = new Programa();
		
		List<Comercializavel> listaDeProdutos = new ArrayList<Comercializavel>();
		
		//Comercializavel c1 = new Comercializavel();
		//Produto p1 = new Produto();

		Navio navio = new Navio();
		navio.setValor(1000);
		Agulha agulha = new Agulha();
		agulha.setValor(10); 
		
		//1a forma: implementação anônina;
		agulha.setAoTocar(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {


				return false;
			}
		});
		
		//2a forma
		agulha.setAoTocar(hospedeira);
		
		//3a forma
		agulha.setAoTocar(new Evento()); 
		
		
		listaDeProdutos.add(navio);
		listaDeProdutos.add(agulha);
		
		imprimirValor(navio);
		imprimirValor(agulha);

	}

	private static void imprimirValor(Comercializavel item) {
		System.out.println("Valor: " + item.getValor()); 
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
}