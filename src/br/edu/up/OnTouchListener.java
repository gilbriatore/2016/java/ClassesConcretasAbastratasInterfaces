package br.edu.up;

public interface OnTouchListener {
	
	boolean onTouch(View v, MotionEvent event);

}
